# frozen_string_literal: true

require 'bitcoin'

module Core
  class BTC
    def initialize(network = :testnet3)
      Bitcoin.network = network
      @is_testnet = network == :testnet3
    end

    def load_address(private_key: '', signature: '')
      key = if private_key.empty?
              generate_address(signature)
            else
              Bitcoin::Key.from_base58(private_key)
            end

      Core::Wallet.new(key.pub, key.priv, key.addr, key.to_base58, @is_testnet)
    end

    private

    def generate_address(signature)
      key = Bitcoin::Key.generate
      sig = key.sign(signature)
      key.verify(signature, sig)
      key
    end
  end
end
