# frozen_string_literal: true

require 'faraday'
require 'json'

module Core
  class Wallet
    SATOSHI_IN_BTC = 100_000_000.0
    attr_reader :public, :private, :address, :base58, :last_input_hash, :funds, :is_testnet

    def initialize(public, private, address, base58, is_testnet)
      @public = public
      @private = private
      @address = address
      @base58 = base58
      @funds = 0
      @is_testnet = is_testnet
      calculate_funds
    end

    def to_s
      "address: #{@address}
available: #{funds}
latest input transaction: #{last_input_hash}"
    end

    private

    def calculate_funds
      txs = current_balance_json

      if txs.any?
        txs.select { |tx| tx[:status][:confirmed] }.each do |tx|
          @funds += tx[:value]
        end
      end

      @last_input_hash = begin
        txs.last[:status][:block_hash]
      rescue StandardError
        ''
      end

      @funds /= Core::Wallet::SATOSHI_IN_BTC
    end

    def connection = Faraday.new('https://blockstream.info')

    def current_balance_json = JSON.parse(connection.get(utxo_path).body,
                                          symbolize_names: true)

    def utxo_path = "#{@is_testnet ? '/testnet' : ''}/api/address/#{@address}/utxo"
  end
end
