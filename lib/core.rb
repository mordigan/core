# frozen_string_literal: true

require_relative 'core/version'
require_relative 'core/btc'
require_relative 'core/wallet'

module Core
  class Error < StandardError; end

  class Program
    attr :btc_lib

    def initialize
      @btc_lib = Core::BTC.new
    end

    def load_address(pk: '', signature: '')
      @btc_lib.load_address(signature: signature, private_key: pk)
    end
  end
end
