# frozen_string_literal: true

require_relative 'lib/core/version'

Gem::Specification.new do |spec|
  spec.name          = 'core'
  spec.version       = Core::VERSION
  spec.authors       = ['urumo']
  spec.email         = ['aramhrptn@hotmail.com']

  spec.summary       = 'Core logic for crypto transfer wizard'
  spec.description   = 'Core gem for both Rails and CLI'
  spec.required_ruby_version = '>= 3.0.0'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'bitcoin-ruby', '~> 0.0.20'
  spec.add_dependency 'faraday'
end
