# Core - a wrapper around bitcoin-ruby
[![forthebadge](https://forthebadge.com/images/badges/made-with-ruby.svg)]()
## Installation
```ruby
 gem 'core', git: 'https://gitlab.com/mordigan/core.git' 
 ``` 
in your Gemfile

## Usage
* First create an instance of `Core` 
```ruby
core = Core::Program.new
```
* Then feed a base58 private key to `.load_address(pk: private_key)` method(`pk` is an optional parameter, if left empty will generate a new wallet)
* `.load_address` returns a `Core::Wallet` class instance
* `Core::Wallet` properties and methods are
* * `:public` => public key of the wallet
* * `:private` => private key of the wallet
* * `:address` => the address of the wallet
* * `:base58` => base58 encoded private key of the wallet
* * `:funds` => amount of BTC you have on this wallet
* * `:last_input_hash` => the hash of the last transaction that sent you BTC
* * `.to_s` => returns a short string with wallet information
